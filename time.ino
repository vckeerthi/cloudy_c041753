// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
InternetButton b = InternetButton();

void setup() {
    b.begin();
    Particle.function("hours",hours);
    Particle.function("minutes",minutes);
    Particle.function("weather",weather);

}
int hours(String cmd){
    int scale = cmd.toInt();
    if(scale >=01 && scale<=12){
        b.allLedsOff();
        b.ledOn(scale,100,0,0);
      for (int i=scale;i>=0;i--){
        b.playNote("G3",8);
        }
        return 1;
    }
    else {
        return 0;
    }
}

int minutes(String cmd){
    int scale = cmd.toInt();
    if(scale >=01 && scale<=10){
        b.allLedsOff();
        b.ledOn(scale,100,100,100);
        return 1;
    }
    else if(scale>=11 && scale <= 19){
        b.allLedsOff();
        b.ledOn(1,100,100,100);
        b.ledOn((scale-10),0,100,100);
        return 2;
    }

     else if(scale>=21 && scale <= 29){
        b.allLedsOff();
        b.ledOn(2,100,100,100);
        b.ledOn((scale-20),0,100,100);
        return 3;
    }
    else if(scale>=31 && scale <= 39){
        b.allLedsOff();
        b.ledOn(3,100,100,100);
        b.ledOn((scale-30),0,100,100);
        return 4;
    }
    else if(scale>=41 && scale <= 49){
        b.allLedsOff();
        b.ledOn(4,100,100,100);
        b.ledOn((scale-40),0,100,100);
        return 5;
    }
    else if(scale>=51 && scale <= 59){
        b.allLedsOff();
        b.ledOn(5,100,100,100);
        b.ledOn((scale-50),0,100,100);
        return 6;
    }
    else if(scale==20 || scale==30 || scale==40 || scale==50||scale==60){
        b.allLedsOff();
        b.ledOn((scale/10),100,100,100);
        b.ledOn(10,0,100,100);
        return 10;
    }
}
int weather(String cmd){
    int scale = cmd.toInt();
   if(scale >=1 && scale<=10){
        b.allLedsOff();
        b.ledOn(scale,100,0,0);
        return 1;
    }
    else if(scale>=11 && scale <= 19){
        b.allLedsOff();
        b.ledOn(1,100,0,0);
        b.ledOn((scale-10),100,0,0);
        return 2;
    }

     else if(scale>=21 && scale <= 29){
        b.allLedsOff();
        b.ledOn(2,100,0,0);
        b.ledOn((scale-20),100,0,0);
        return 3;
    }
    else if(scale>=31 && scale <= 39){
        b.allLedsOff();
        b.ledOn(3,100,0,0);
        b.ledOn((scale-30),100,0,0);
        return 4;
    }
    else if(scale>=41 && scale <= 49){
        b.allLedsOff();
        b.ledOn(4,0,0,0);
        b.ledOn((scale-40),100,0,0);
        return 5;
    }

    else if(scale==20 || scale==30 || scale==40 || scale==50){
        b.allLedsOff();
        b.ledOn((scale/10),100,0,0);
        b.ledOn(10,100,0,0);
        return 10;
    }
    else if(scale <= -1 && scale>= -10){
        b.allLedsOff();
        b.ledOn(scale,0,0,100);
        return -1;
    }
    else if(scale<= -11 && scale >= -19){
        b.allLedsOff();
        b.ledOn(1,0,0,100);
        b.ledOn((scale-10),0,0,100);
        return -2;
    }

     else if(scale<= -21 && scale >= -29){
        b.allLedsOff();
        b.ledOn(2,0,0,100);
        b.ledOn((scale-20),0,0,100);
        return -3;
    }
    else if(scale<= -31 && scale >= -39){
        b.allLedsOff();
        b.ledOn(3,100,100,100);
        b.ledOn((scale-30),0,0,100);
        return -4;
    }
    else if(scale<= -41 && scale >= -49){
        b.allLedsOff();
        b.ledOn(4,100,100,100);
        b.ledOn((scale-40),0,0,100);
        return -5;
    }

    else if(scale== -20 || scale== -30 || scale== -40 || scale== -50){
        b.allLedsOff();
        b.ledOn((scale/10),0,0,100);
        b.ledOn(10,0,100,100);
        return -10;
    }
    else if(scale == 0){
        b.allLedsOff();
        b.ledOn(11,100,100,100);
    }


}
