//
//  ViewController.swift
//  Activity3
//
//  Created by Chandra Keerthi on 2019-11-04.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit
import Particle_SDK
import Alamofire
import WatchConnectivity

class ViewController: UIViewController ,WCSessionDelegate{
   func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
          
      }
      
      func sessionDidBecomeInactive(_ session: WCSession) {
          
   }
      
      func sessionDidDeactivate(_ session: WCSession) {
          
      }

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    var temp:String!
    var timehr:String!
    var timemin:String!
    var city:String!
     var session:WCSession!
    
    
    
    
    let USERNAME = "c0741753@mylambton.ca"
        let PASSWORD = "Bhargavi@2510"
        let DEVICE_ID = "44002c000f47363333343437"
        var myPhoton : ParticleDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       if (WCSession.isSupported()) {
           print("PHONE: Phone supports WatchConnectivity!")
           self.session = WCSession.default
           self.session.delegate = self
           self.session.activate()
       }
       else {
           print("PHONE: Phone does not support WatchConnectivity")
       }
    setupParticle()
        getDeviceFromCloud()

       
    }
    
   func session(_ session: WCSession,
                didReceiveMessage message: [String : Any],
                replyHandler: @escaping ([String : Any]) -> Void) {
     DispatchQueue.main.async {
        self.timehr = message["timehr"] as! String
        self.timemin = message["timemin"] as! String
        self.temp = message["temp"] as! String
        self.city = message["city"] as! String

        self.cityLabel.text = "city:\((self.city)!)"
        self.timeLabel.text = "time:\((self.timehr)!):\((self.timemin)!)"
        self.tempLabel.text = "temp:\((self.temp)!)"

    }

   }
    func setupParticle(){
         
             ParticleCloud.init()
               
             ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
             if (error != nil) {
                            
                 print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                             
                 print(error?.localizedDescription)
                          }
             else {
                 print("Login success!")
                 }
         }
     }
     
     
     func getDeviceFromCloud() {
     ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
             
             if (error != nil) {
                 print("Could not get device")
              print(error?.localizedDescription)
                 return
             }
             else {
                 print("Got photon: \(device?.id)")
                 self.myPhoton = device
             }
             
         }
     }
    

    @IBAction func showHoursPressed(_ sender: Any) {
        let funcArgs = [(self.timehr)!] as [Any]
               var task = myPhoton!.callFunction("hours", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                   if (error == nil) {
                       print("LEDs successfully turned on")
                   }
               }
    }
    @IBAction func showminsPressed(_ sender: Any) {
        let funcArgs = [(self.timemin)!] as [Any]
               var task = myPhoton!.callFunction("minutes", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                   if (error == nil) {
                       print("LEDs successfully turned on")
                
               }
        }
    }
    
    @IBAction func showweatherPressed(_ sender: Any) {
        let funcArgs = [self.temp!] as [Any]
                    var task = myPhoton!.callFunction("weather", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
                        if (error == nil) {
                            print("LEDs successfully turned on")
                        }
        }
    }
}


