//
//  InterfaceController.swift
//  Activity3 WatchKit Extension
//
//  Created by Chandra Keerthi on 2019-11-04.
//  Copyright © 2019 student. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import SwiftyJSON
import WatchConnectivity

class InterfaceController: WKInterfaceController,WCSessionDelegate{

    var lat:String!
    var lon:String!
    
    var temperature : String!
    var timehr: String!
    var timemin:String!
    var cityname: String!
    var hours: String!
    var minutes: String!
    @IBOutlet weak var timeLabel: WKInterfaceLabel!
    @IBOutlet weak var tempLabel: WKInterfaceLabel!
    @IBOutlet weak var cityLabel: WKInterfaceLabel!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func latPressed() {
        let suggestedResponses = ["48.8566", "40.7128","43.6532","28.7041"]
        presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                
                self.lat = userResponse!
           
            }
        }
    }
    @IBAction func lonPressed() {
        let suggestedResponses = ["2.3522", "74.0060","79.3832","77.1025"]
               presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {
                   
                   (results) in
                   
                   if (results != nil && results!.count > 0) {
                       // 2. write your code to process the person's response
                       let userResponse = results?.first as? String
                       
                       self.lon = userResponse!
                   }
               }
           
    }
    @IBAction func checkPressed() {
        AF.request("https://api.darksky.net/forecast/f23d5d50115739bf9d5cb8efcf281310/\((self.lat)!),\((self.lon)!)").responseJSON {
                    (xyz) in
                    let x = JSON(xyz.value)
                    let time = x["currently"]["time"].double!
                    let unixTimestamp = time
                    let date = Date(timeIntervalSince1970: unixTimestamp)
                    let calender = Calendar.current
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeZone = TimeZone(abbreviation: "GMT")//Set timezone that you want
                    
                    dateFormatter.dateFormat = "HH" //Specify your format that you want
                    let strDate = dateFormatter.string(from: date)
            
                let dateformat = DateFormatter()
            dateformat.timeZone = TimeZone(abbreviation: "GMT")
            dateformat.dateFormat = "mm"
            let minDate = dateFormatter.string(from: date)
                    
            let temp = x["currently"]["temperature"].double
            let tempCelsius:Double = (temp! - 32.00)*(5/9)
                    let city = x["timezone"]
      
        var tempStr = String(format: "%.0f", tempCelsius)
        
            self.timeLabel.setText("time: \(strDate):\(minDate)")
            self.cityLabel.setText("city:\(city)")
            self.tempLabel.setText("temp:\(tempStr)")
            self.timehr = "\(strDate)"
            self.timemin = "\(minDate)"
            self.cityname = "\(city)"
            self.temperature = "\(tempStr)"
            
        }
        
       
        
    }
    
    @IBAction func sendPressed() {
        if WCSession.default.isReachable {
    print("Attempting to send message to phone")
    WCSession.default.sendMessage(
        ["timehr":"\((self.timehr)!)","timemin":"\((self.timemin)!)","temp":"\((self.temperature)!)","city":"\((self.cityname)!)"],
        replyHandler: {
            (_ replyMessage: [String: Any]) in
            // @TODO: Put some stuff in here to handle any responses from the PHONE
            print("Message sent, put something here if u are expecting a reply from the phone")
    }, errorHandler: { (error) in
        //@TODO: What do if you get an error
        print("Error while sending message: \(error)")
    })
}
else {
    print("Phone is not reachable")
        }
    }
       
    
}
//extension Float {
//    var clean: String {
//        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
//    }
//}
